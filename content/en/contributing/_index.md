---
title: "Feedback"
anchor: "contributing"
weight: 70
---

The [website is open-sourced](https://gitlab.com/20_games_challenge/20_games_challenge.gitlab.io), so any feedback, suggestions, or contributions are appreciated!  
(I didn't spend a lot of time on this page, but I'll try to make things more streamlined in the future if there is enough interest.)

## Submitting a game
Don't see your favorite game on the list? Feel free to send me a message or add it yourself.

## Game Details Page
All difficulty assessments and definitions of done are estimates (with the exception of games I've made myself), so feel free to let me know if you disagree with an estimate.

If you have created a game for yourself, please let me know how it went, and if there are any parts of the game page that I should update.

## Getting featured
Each game page has a section for tutorials and devlogs. If you made a blog post or video about a game in the challenge and want to be added to the list, then please let me know.  
(Disclaimer: I won't promise that everything will be included as I want to emphasize quality, but I will at least consider adding anything that fits with the challenge.)

## Contact me
I have a [Discord server](https://discord.gg/ZZ3BP3abRg) if you want to get in touch.  
I also sometimes remember to check Twitter [@Matt_SDG_Games](https://twitter.com/Matt_SDG_Games).  
I'm migrating my email to a new domain, but I should have an email link here as well soon.