---
title: "The Challenge"
anchor: "games"
geekdocCollapseSection: true
weight: 30
---

Here is a list of 10 games to get you started. There are couple of alternatives, so you can select either game from each section. Each game will be slightly larger and more difficult than the game from the previous section, so you will always be learning and growing as you make more and more games.

Each game in the list below will have a short description, as well as a Definition of Done and a difficulty estimate. Click on the game name to see its details.

{{< expand "Click here to see what each part of a game's description means. (example game)">}} {{< include file="_parts/example.md" type=page >}} {{< /expand >}}

### 1.
{{< tabs "game1" >}}
{{< tab "Game #1" >}}
  {{< hint type=note title="Start here" >}}
  Select a game from the tabs above. You only have to make one of the games. If you can't decide, just make Pong. After you have made this game, move on to ***"Game #2"*** below.
  {{< /hint >}}

  Step 1: Make a game!

  Gain a basic understanding of your game engine/editor and put together a complete (but small) game! <br> Don't worry about making art just yet, you can find a lot of sprites online that you can use. The main focus for now is on the mechanics of the game engine itself.

  ***Things that you will learn:***
  * Using your game engine of choice! 
  * Creating and destroying objects.
  * Processing player input and moving objects on screen.
  * Detecting and reacting to collisions.
{{< /tab >}}
{{< tab "Pong" >}} {{< include file="games/pong.md" type=page  >}} {{< /tab >}}
{{< tab "Flappy Bird" >}} {{< include file="games/flappy.md" type=page >}} {{< /tab >}}
{{< /tabs >}}

### 2.
{{< tabs "game2" >}}
{{< tab "Game #2" >}}

  Step 2: Make a slightly more complex game.

  You just finished your first game! Take a moment to make something else of similar scope before we move on to more complex concepts.
  
  ***Things that you will learn:***
  * Re-use code and assets between projects
  {{< hint type=tip title="You don't have to start over!" >}}
  Did you notice the similarity between Pong and Breakout, or Flappy Bird and Jetpack Joyride? You can re-use some of the code and assets that you made in the previous game.
  
  If your previous solution is too messy, then you might just want to start over. You can save yourself a lot of work if you know when (and how) to re-use existing assets in new games. Not everything is worth saving; but you don't have to always start over, either.
  {{< /hint >}}
  * Save a high score between play sessions.
  * Create and play simple sound effects (if you didn't in the previous game!)

{{< /tab >}}
{{< tab "Breakout" >}} {{< include file="games/breakout.md" type=page >}} {{< /tab >}}
{{< tab "Jetpack Joyride" >}} {{< include file="games/jetpack.md" type=page >}} {{< /tab >}}
{{< /tabs >}}


### 3.
{{< tabs "game3" >}}
{{< tab "Game #3" >}}

  Step 3: Make some art!

  This game is similar in mechanical scope to game #2. Take the opportunity to focus on the game sprites. Try your hand at animating a moving sprite across multiple frames.  
  If you are interested in composing your own sound tracks, then this is a great time to focus on that skill as well.
  
  Finally, this is a great chance to play with particle systems. Make some particle effects that you can play when things explode (or when a frog is run over...)

  ***Things that you will learn:***
  * 2D sprite art
  * Game Music
  * Particle Effects

{{< /tab >}}
{{< tab "Space Invaders" >}} {{< include file="games/invaders.md" type=page >}} {{< /tab >}}
{{< tab "Frogger" >}} {{< include file="games/frogger.md" type=page >}} {{< /tab >}}
{{< tab "Moon Patrol" >}} {{< include file="games/moon_patrol.md" type=page >}} {{< /tab >}}
{{< /tabs >}}

### 4.
{{< tabs "game4" >}}
{{< tab "Game #4" >}}

  Step 4: Make a more advanced character controller!

  Say goodbye to simple controls! This game will feature a top-down character controller with physics and multiple axis of movement.
  
  This is also a good time to start working on menus and UI design. This game should have a complete main menu, as well as a game over/restart menu.

  ***Things that you will learn:***
  * 2D sprite rotation.
  * 2D Vector Physics, applying a force in the direction of rotation.
  * Make a main menu and a restart menu.

{{< /tab >}}
{{< tab "Asteroids" >}} {{< include file="games/asteroids.md" type=page >}} {{< /tab >}}
{{< tab "Spacewar!" >}} {{< include file="games/spacewar.md" type=page >}} {{< /tab >}}
{{< tab "Indy 500" >}} {{< include file="games/indy.md" type=page >}} {{< /tab >}}
{{< /tabs >}}

{{< hint type=warning title="Pardon the mess" >}}
I really wanted to get this resource up in time for the new year, but there's still a little housekeeping to do.  

I'm adding details to the games below, but it's possible that games might change in order to best balance the challenge.  
I hope to be done with this part in another day or two.
{{< /hint >}}

### 5.
{{< tabs "game5" >}}
{{< tab "Game #5" >}}

  Step 5: Make a basic AI!

  ***Things that you will learn:***
  * Make an AI
  * Making a Menu

{{< /tab >}}
{{< tab "Pac Man" >}} {{< include file="games/pacman.md" type=page >}} {{< /tab >}}
{{< tab "Super Sprint" >}} {{< include file="games/asteroids.md" type=page >}} {{< /tab >}}
{{< tab "Tic-Tac-Toe" >}} {{< include file="games/tic_tac_toe.md" type=page >}} {{< /tab >}}
{{< /tabs >}}

### 6.
{{< tabs "game6" >}}
{{< tab "Game #6" >}}

  Step 6: Learn to work with data sets!

  ***Things that you will learn:***
  * Data structures
  * Gridmaps

{{< /tab >}}
{{< tab "Tetris" >}} {{< include file="games/.md" type=page >}} {{< /tab >}}
{{< tab "Bejeweled" >}} {{< include file="games/.md" type=page >}} {{< /tab >}}
{{< tab "Snake" >}} {{< include file="games/.md" type=page >}} {{< /tab >}}
{{< /tabs >}}

### 7.
{{< tabs "game7" >}}
{{< tab "Game #7" >}}

  Step 7: Make a simple platformer!

  ***Things that you will learn:***
  * Platformer character controller
  * Basic Vector Math
  * Level Creation (gridmaps?)

{{< /tab >}}
{{< tab "Mario" >}} {{< include file="games/.md" type=page >}} {{< /tab >}}
{{< tab "Pitfall" >}} {{< include file="games/.md" type=page >}} {{< /tab >}}
{{< tab "VVVVVV" >}} {{< include file="games/.md" type=page >}} {{< /tab >}}
{{< /tabs >}}

### 8.
{{< tabs "game8" >}}
{{< tab "Game #8" >}}

  Step 8:

  ***Things that you will learn:***
  * Terrain Manipulation
  * Advanced 2D physics

{{< /tab >}}
{{< tab "Worms" >}} {{< include file="games/.md" type=page >}} {{< /tab >}}
{{< tab "Lemmings" >}} {{< include file="games/.md" type=page >}} {{< /tab >}}
{{< tab "Dig Dug" >}} {{< include file="games/.md" type=page >}} {{< /tab >}}
{{< tab "Motherload" >}} {{< include file="games/.md" type=page >}} {{< /tab >}}
{{< /tabs >}}

### 9.
{{< tabs "game9" >}}
{{< tab "Game #9" >}}
  
  Step 9: Let's enter the 3rd dimension!
  
  ***Things that you will learn:***
  * Introduction to 3D
  * 2.5D game mechanics
  * Basic 3D modeling.

{{< /tab >}}
{{< tab "Super Monkeyball" >}} {{< include file="games/.md" type=page >}} {{< /tab >}}
{{< tab "Star Fox" >}} {{< include file="games/.md" type=page >}} {{< /tab >}}
{{< tab "Crash Bandicoot" >}} {{< include file="games/.md" type=page >}} {{< /tab >}}
{{< /tabs >}}

### 10.
{{< tabs "game10" >}}
{{< tab "Game #10" >}}

  Step 10: Make a fully 3D game!

  ***Things that you will learn:***
  * Full 3D game
  * Quaternion Rotation
  * Billboarding*

{{< /tab >}}
{{< tab "Wolfenstein or Doom" >}} {{< include file="games/.md" type=page >}} {{< /tab >}}
{{< tab "Mario Kart" >}} {{< include file="games/.md" type=page >}} {{< /tab >}}
{{< tab "Minecraft" >}} {{< include file="games/.md" type=page >}} {{< /tab >}}
{{< /tabs >}}

## Game 11 and Beyond

Congratulations on completing your 10th game! From this point onwards, the list is in your hands. You know what type of game you're interested in making. Try making games that teach you the specific skills you will need. For example, if you want to make real-time strategy games, then you could learn a lot from tower defense games. Take a look at the [full list]({{< ref "games/_index.md" >}}) (but feel free to deviate if there's a different game you want to try.)

{{< expand " ">}}

Nothing to see here...  
(Stuff I'm still using while finishing the website)

#### Advanced Concepts
  - Configuration Screen
  - Level Editor
  - Networking
  - Multiplayer Games
  - Advanced pathfinding (RTS)
  - Advanced behavioral AI (Sims)
  - Adding custom tools to the editor
  - Making custom engine builds
  - Linking engine with external libraries
  - Creating custom C# libraries
  - Localization

#### Level 3
  - GDExtension optimization
  - Inverse Kinematics
  - 3d modeling
  - 3d animation
  - Player and NPC animation
  - Mastering Nodes
  - Smooth Scene Transitions
  - Custom UI
  - Save and Load Screen
  - Environments and Lighting
  - Particles
  - Sound Effects, Music and audio buses
  - Viewports, 2D in 3D environments
  - Advanced Vector Math
  - 3D gimbals
  - NPC AIs
  - Dialogs, Inventory
  - Timer Nodes
  - 2D Boolean (cutting holes in things)
  - 3d Boolean?

#### Level 4
  - Procedural Terrain Generation
  - Voxels
  - Procedural Content
  - Shaders

{{< /expand >}}
