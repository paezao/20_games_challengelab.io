---
title: "How it Works"
anchor: "how"
weight: 20
---

The goal of the 20 games challenge is to gradually learn more about game design through a series of small projects. <br>
At the end of the challenge, you should be comfortable creating games in your engine of choice without a tutorial.

{{< hint type=note title="This is not a tutorial. Instead, you should think of it more like a syllabus." >}}
If you do everything yourself during the challenge, then you will have practice estimating, planning, programming, and polishing your game. You'll also be able to create the music, sound effects, 2D, and 3D art that you'll need.

Each of these skills can take a lifetime to master, but you will at least be familiar with every major aspect of video game design, and you will be able to estimate the scope of any game you want to make in the future.
{{< /hint >}}


## Prerequisites:

You don't need any special skills or training to get started making games! If you have never made a game before, then there are a few things you might want to do before you jump in:

1. {{< expand "Decide on an engine (maybe?)" >}}
Did you ever boot up a brand new game that you've never played before, and the first thing you have to do is select a difficulty rating?

Yeah, it's the same thing here. The good news is, you can always change your mind.

I'd recommend [Godot](https://godotengine.org/), though [Unity](https://unity.com/) is another good choice. If you know that you want to make 3D games only, then [Unreal](https://www.unrealengine.com/en-US/) is a great option. It's also "hard" mode, so be warned.

There are also more "educational" engines like [Scratch](https://scratch.mit.edu/). If you go this way, you will have to chose another engine later if you intend to make commercial games.

If you can't chose, then you might want to make a couple of small games in each engine. Just know that things will get easier as you learn more, so the second engine you use might feel better than the first by default.

In conclusion, any major engine is a good choice, but I'd recommend [Godot](https://godotengine.org/) 🙃

{{< /expand >}}

2. {{< expand "Start with a tutorial if you need it." >}}

As I said above, the 20 Games challenge is not a tutorial in itself. You don't need to know much to get started.

Each engine has its own officially recommended tutorial series, so that might be a good place to start.  
(I might add some extra links here. For now, Google is your friend)

Basically, you need to feel comfortable making a simple game like Pong. You don't need to know how to make a complex game.

{{< hint type=tip title="Doing things twice is a great way to start." >}}
If making a game without a tutorial seems daunting, then you can always follow a tutorial once.<br>
When you finish, create a brand new project and make the same game a second time on your own.<br>
You shouldn't need to do this for every game, but it's OK to do this a few times when you are first starting out.
{{< /hint >}}
{{< /expand >}}

## The challenge:

#### Make up to 20 games on your own. 

1. Start with 10 of the recommended games (or a game of similar scope). Each game will build on the fundamentals while introducing new concepts. By the end of game 10, you should be able to make a 2D or simple 3D game.  
{{< hint type=important title="You don't have to do everything!" >}}
Some games are conceptually simple, but contain a lot of content. For these games, you might want to make a single level instead of the entire game. This is called a ***vertical slice.***  
Check each game's ***definition of done*** for more details.
{{< /hint >}}

2. At this point, you should know how to make a simple game. There are still a lot of advanced concepts to learn, though. The second set of games are directed at expanding your knowledge.
    * Try to come up with a goal or theme for yourself, and pick games that will help you reach your goal.

    * The goal of the challenge is to ***learn how to make games in 20 games or less,*** so you might be ready to start a different project after only 10 or 15 games.

3. End with a capstone! Make a game that is either 1. larger than before or 2. more difficult to complete, and take some extra time to polish it up. This is your chance to show off everything you've learned during the challenge.


# Additional Rules:
#### 1. No tutorials are allowed!
Looking up specific questions (how to move an object, how to save to disk, etc.) is fair game, though. After all, the single most important skill for any game developer is ***the ability to find answers online.***

Every rule does have exceptions, so don't feel bad if you need to break it. Understand, though, that following steps one at a time is not necessarily the same thing as learning. If you need to watch a tutorial, watch it in its entirety, then return to your game. This will ensure that you are still putting in the work needed to learn and grow.

#### 2. Constrain Yourself!
This isn't a game jam, so you don't need to make a game in a single weekend. However, I would highly recommend setting a time limit so each game doesn't take a year to complete. You can always polish a project more, but setting constraints and moving on is a good skill to learn.  

I would recommend 20 hours (of actual development time) as a starting time limit for a project, but you might need more time than that depending on your prior knowledge and skill level.  
Later (larger) projects will take longer to complete simply due to the amount of content larger games contain.

#### 3. Be Flexible!
You should start out with one of the recommended games. After that, feel free to try new things and challenge yourself.

Start out with a rough plan, but be willing to personalize the challenge and substitute different projects in as you go. You'll learn new things, and you might have some new ideas you want to experiment with after a while.

## That's all!
Jump to the [challenge page]({{< ref "challenge/_index.md" >}}) to get started!