---
title: "List of Games"
anchor: "games"
geekdocCollapseSection: true
weight: 40
---

Below, you will find a list of every game that I thought to add to the challenge. If you think that a game is missing, feel free to check the [contributing]({{< ref "contributing/_index.md" >}}) page to get it added!  

**I'm still adding details for the majority of these games** 

Each game in the list below will have a short description, as well as a Definition of Done and a difficulty estimate. Click on the game name to see its details.

{{< expand "See an example">}} {{< include file="_parts/example.md" type=page >}} {{< /expand >}}

This part of the site is under construction. Don't worry, it should be done before you need it!  
The [challenge page]({{< ref "challenge/_index.md" >}}) is a good place to start in the meantime.

# Game List:
| **Game**                                | **Complexity** | **Size** | **Platform**      | **Release Year** |
| :---                                    | :---           | :--      | :---              | :---             |
| Pong                                    | 0.0            | 0.0      | Arcade, Console   | 1972             |
| Flappy Bird                             | 0.0            | 0.0      |                   | 2013             |
| Doodle Jump                             | 0.0            | 0.0      |                   | 2009             |
| Breakout                                | 0.0            | 0.0      | Arcade, Console   | 1976             |
| Jetpack Joyride                         | 0.0            | 0.0      |                   | 2011             |
| Mega Jump                               | 0.0            | 0.0      |                   | 2010             |
| Space Invaders                          | 0.0            | 0.0      | Arcade            | 1978             |
| Frogger                                 | 0.0            | 0.0      | Arcade            | 1981             |
| Moon Patrol                             | 0.0            | 0.0      | Arcade            | 1982             |
| Pitfall                                 | 0.0            | 0.0      | Console           | 1982             |
| River Raid                              | 0.0            | 0.0      | Console           | 1982             |
| Asteroids                               | 0.0            | 0.0      | Arcade            | 1979             |
| Pac-Man                                 | 0.0            | 0.0      | Arcade            | 1980             |
| Cookie Clicker                          | 0.0            | 0.0      | Computer          | 2013             |
| Mario Bros                              | 0.0            | 0.0      | Arcade            | 1983             |
| Sonic the Hedgehog                      | 0.0            | 0.0      | Console           | 1991             |
| Worms                                   | 0.0            | 0.0      | Computer          | 1995             |
| Super Motherload                        | 0.0            | 0.0      | Computer          | 2013             |
| Wolfenstein 3D (Doom?)                  | 0.0            | 0.0      | Computer          | 1992             |
| Minecraft                               | 0.0            | 0.0      | Computer          | 2009             |
| Mario Kart                              | 0.0            | 0.0      | Console           | 1992             |
| Star Fox                                | 0.0            | 0.0      | Console           | 1993             |
| Wipeout (F0)                            | 0.0            | 0.0      | Console           | 1995             |
| Dune II                                 | 0.0            | 0.0      | Computer          | 1992             |
| Sim City                                | 0.0            | 0.0      | Computer          | 1989             |
| The Sims                                | 0.0            | 0.0      | Computer          | 2000             |
| Lemmings                                | 0.0            | 0.0      | Computer          | 1991             |
| Blokade (Snake)                         | 0.0            | 0.0      | Arcade            | 1976             |
| Pole Position                           | 0.0            | 0.0      | Arcade            | 1982             |
| Tetris                                  | 0.0            | 0.0      | Console, Handheld | 1984             |
| Kareteka                                | 0.0            | 0.0      | Computer          | 1984             |
| Pocket Tanks                            | 0.0            | 0.0      |                   | 2001             |
| Peggle                                  | 0.0            | 0.0      | Computer          | 2007             |
| Portal                                  | 0.0            | 0.0      | Computer          | 2007             |
| Maze War                                | 0.0            | 0.0      | Computer          | 1974             |
| Zork                                    | 0.0            | 0.0      | Computer          | 1980             |
| Rogue                                   | 0.0            | 0.0      | Computer          | 1980             |
| BattleZone (3D tank game)               | 0.0            | 0.0      | Arcade            | 1980             |
| Missile Command                         | 0.0            | 0.0      | Arcade            | 1980             |
| Zaxxon                                  | 0.0            | 0.0      | Arcade            | 1981             |
| Donkey Kong                             | 0.0            | 0.0      | Arcade            | 1981             |
| Pitfall                                 | 0.0            | 0.0      | Arcade            | 1982             |
| Burger Time                             | 0.0            | 0.0      | Console           | 1982             |
| Dig Dug                                 | 0.0            | 0.0      | Arcade            | 1982             |
| Mappy                                   | 0.0            | 0.0      | Arcade            | 1983             |
| Elevator Action                         | 0.0            | 0.0      | Arcade            | 1983             |
| Ice Climber                             | 0.0            | 0.0      | Arcade            | 1984             |
| Marble Madness                          | 0.0            | 0.0      | Arcade            | 1984             |
| Spy Vs Spy                              | 0.0            | 0.0      | Console           | 1984             |
| Rampage                                 | 0.0            | 0.0      | Arcade            | 1986             |
| Prince of Persia                        | 0.0            | 0.0      | Computer          | 1989             |
| Super Mario Bros                        | 0.0            | 0.0      | Console           | 1985             |
| The Legend of Zelda                     | 0.0            | 0.0      | Console           | 1986             |
| Snail Maze                              | 0.0            | 0.0      | Console           | 1986             |
| Minesweeper                             | 0.0            | 0.0      | Computer          | 1989             |
| Secret of Monkey Island                 | 0.0            | 0.0      | Console           | 1990             |
| Doom                                    | 0.0            | 0.0      | Computer          | 1993             |
| Full Tilt Pinball                       | 0.0            | 0.0      | Computer          | 1995             |
| Command & Conquer                       | 0.0            | 0.0      | Computer          | 1995             |
| Super Mario 64                          | 0.0            | 0.0      |                   | 1996             |
| Pokémon                                 | 0.0            | 0.0      |                   | 1996             |
| Quake                                   | 0.0            | 0.0      | Computer          | 1996             |
| Diablo                                  | 0.0            | 0.0      | Computer          | 1997             |
| Gran Turismo                            | 0.0            | 0.0      |                   | 1997             |
| Star Craft                              | 0.0            | 0.0      | Computer          | 1998             |
| Roller Coaster Tycoon                   | 0.0            | 0.0      |                   | 1999             |
| Bejeweled                               | 0.0            | 0.0      | Computer          | 2001             |
| Guitar Hero                             | 0.0            | 0.0      |                   | 2005             |
| Line Rider                              | 0.0            | 0.0      | Computer          | 2006             |
| Double Dragon                           |                |          | Arcade            |                  |
| Wii Sports                              | 0.0            | 0.0      |                   | 2006             |
| Bloxorz                                 | 0.0            | 0.0      | Web               | 2007             |
| Tiny Wings                              | 0.0            | 0.0      |                   | 2011             |
| FTL: Faster Than Light                  | 0.0            | 0.0      | Computer          | 2012             |
| Hill Climb Racing                       | 0.0            | 0.0      |                   | 2012             |
| Rocket League                           | 0.0            | 0.0      |                   | 2015             |