---
title: "Space Invaders"
date: 2018-01-27T15:42:17+01:00
anchor: "invaders"
weight: 5
---

Here is some information about the game.

| ***Difficulty*** |                                              |
| :---             | :---                                         |
| Complexity       | {{< icon "star" >}} {{< icon "star-half" >}} |
| Scope            | {{< icon "star" >}} {{< icon "star" >}} | 

### Goal:
* 

### Stretch goal:
* 

### Devlogs and tutorials
Coming soon!