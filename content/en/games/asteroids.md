---
title: "Asteroids"
date: 2018-01-27T15:42:17+01:00
anchor: "asteroids"
weight: 6
---

Here is some information about the game.

| ***Difficulty*** |                                              |
| :---             | :---                                         |
| Complexity       | {{< icon "star" >}} {{< icon "star-half" >}} |
| Scope            | {{< icon "star" >}} {{< icon "star" >}} | 

### Goal:
* 

### Stretch goal:
* 

### Devlogs and tutorials
Coming soon!